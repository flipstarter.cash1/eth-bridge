# Eth Bridge

Bridge and ETH.RUNE - ERC20 contract

## ETH.RUNE Design 

Ownable, Mintable, Burnable ERC20. 

Max Supply of 500m (BNB.RUNE Supply)

10m RUNE minted on construction. Owner can mint more if needed to control supply. 

ETH.RUNE is intended only to be a transitionary asset to be upgraded to native THOR.RUNE. Users should not hold ETH.RUNE indefinitely. 

## Bridge Design 

Owner can set a server, RUNE or owner. 

Server or Owner can move out funds. 

User can deposit RUNE. 

**Public Functions**
```solidity
// User calls to deposit asset with a memo
function deposit(uint value, string memory memo) public
```

### Testing

Uses Buidler + Web3 + Ethers + Waffle

```bash
npx buidler test
```