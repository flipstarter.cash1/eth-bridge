/*
* This test deploys the BRIDGE & RUNE from User address
* It then updates OWNER to an OWNER address
* It then tests for User to Deposit in Assets, which gets forwarded to OWNER
* It then tests for OWNER to send out those assets back to User
*/

const Bridge = artifacts.require("Bridge");
const Rune = artifacts.require("ETH_RUNE");
const BigNumber = require('bignumber.js')
const truffleAssert = require('truffle-assertions')

function BN2Str(BN) { return ((new BigNumber(BN)).toFixed()) }

var OWNER; var USER;
var BRIDGE;
var RUNE;
var _1m = '1000000000000000000000000'
var _2m = '2000000000000000000000000'
var _9m = '9000000000000000000000000'
var _10m = '10000000000000000000000000'
var _500m = '500000000000000000000000000'

describe("RUNE contract", function () {
  let accounts;

  before(async function () {
    accounts = await web3.eth.getAccounts();
    RUNE = await Rune.new();
    OWNER = accounts[0]
    USER = accounts[1]
  });

  describe("Construction", function () {

    it("Should set up RUNE properly", async function () {
      assert.equal(await RUNE.name(), 'THORChain ETH.RUNE')
      assert.equal(await RUNE.symbol(), 'RUNE')
      assert.equal(await RUNE.decimals(), '18')
      assert.equal(await RUNE.totalSupply(), 10*10**6 * 10**18)
      assert.equal(await RUNE.maxSupply(), 500*10**6 * 10**18)
    });

  });

  describe("ERC20 Functions", function () {

    it("Should transfer RUNE", async function () {
      await RUNE.transfer(USER, _1m, { from: OWNER }),
      expect(BN2Str(await RUNE.balanceOf(OWNER))).to.equal(_9m);
      expect(BN2Str(await RUNE.balanceOf(USER))).to.equal(_1m);
    });

    it("Should fail to mint too much", async function () {
      await truffleAssert.fails(
        RUNE.mint(_500m, { from: OWNER }),
        truffleAssert.ErrorType.REVERT, "Must be less than maxSupply");
    });
    it("Should fail to mint from user", async function () {
      await truffleAssert.fails(
        RUNE.mint(_500m, { from: USER }),
        truffleAssert.ErrorType.REVERT, "caller is not the owner");
    });

    it("Should mint more RUNE", async function () {
      await RUNE.mint(_1m, { from: OWNER }),
      assert.equal(await RUNE.totalSupply(), 11*10**6 * 10**18)
      expect(BN2Str(await RUNE.balanceOf(OWNER))).to.equal(_10m);
    });

    it("Should approve/transferFrom RUNE", async function () {
      expect(BN2Str(await RUNE.balanceOf(OWNER))).to.equal(_10m);
      await RUNE.approve(USER, _1m, { from: OWNER })
      expect(BN2Str(await RUNE.allowance(OWNER, USER))).to.equal(_1m);

      await RUNE.transferFrom(OWNER, USER, _1m, { from: USER })
      expect(BN2Str(await RUNE.allowance(OWNER, USER))).to.equal('0');
      expect(BN2Str(await RUNE.balanceOf(OWNER))).to.equal(_9m);
      expect(BN2Str(await RUNE.balanceOf(USER))).to.equal(_2m);
    });

    it("Should burn RUNE", async function () {
      await RUNE.burn(_1m, { from: USER }),
      assert.equal(await RUNE.totalSupply(), 10*10**6 * 10**18)
      expect(BN2Str(await RUNE.balanceOf(USER))).to.equal(_1m);
    });
  });

  describe("Ownership Functions", function () {

    it("Should transfer owner", async function () {
      await RUNE.transferOwnership(USER, { from: OWNER }),
      expect(await RUNE.getOwner()).to.equal(USER);
    });
    it("Should purge owner", async function () {
      await RUNE.renounceOwnership({ from: USER }),
      expect(await RUNE.getOwner()).to.equal('0x0000000000000000000000000000000000000000');
    });

  });
  
});
