/*
* This test deploys the BRIDGE & RUNE from User address
* It then updates OWNER to an OWNER address
* It then tests for User to Deposit in Assets, which gets forwarded to OWNER
* It then tests for OWNER to send out those assets back to User
*/

const Bridge = artifacts.require("Bridge");
const Rune = artifacts.require("ETH_RUNE");
const BigNumber = require('bignumber.js')
const truffleAssert = require('truffle-assertions')

function BN2Str(BN) { return ((new BigNumber(BN)).toFixed()) }

var OWNER; var SERVER; var USER; var USER2;
var BRIDGE;
var RUNE;
var _1m = '1000000000000000000000000'

describe("BRIDGE contract", function () {
  let accounts;

  before(async function () {
    accounts = await web3.eth.getAccounts();
    BRIDGE = await Bridge.new();
    RUNE = await Rune.new();
    OWNER = accounts[0]
    SERVER = accounts[1]
    USER = accounts[2]
    USER2 = accounts[3]
    await RUNE.transfer(USER, _1m, { from: OWNER })
  });

  describe("Construction", function () {

    it("Should set up RUNE properly", async function () {
      assert.equal(await RUNE.name(), 'THORChain ETH.RUNE')
      assert.equal(await RUNE.symbol(), 'RUNE')
      assert.equal(await RUNE.decimals(), '18')
      assert.equal(await RUNE.totalSupply(), 10*10**6 * 10**18)
      assert.equal(await RUNE.maxSupply(), 500*10**6 * 10**18)
    });

    it("Should set up BRIDGE properly", async function () {
      assert.equal(await BRIDGE.owner(), OWNER)
    });
  });

  describe("Add SERVER", function () {

    it("Should fail to add OWNER from wrong account", async function () {
      await truffleAssert.fails(
        BRIDGE.setServer(SERVER, { from: accounts[4] }),
        truffleAssert.ErrorType.REVERT, "Must be Owner");
    });
    it("Should add SERVER", async function () {
      let tx = await BRIDGE.setServer(SERVER, { from: OWNER });
      expect(await BRIDGE.server()).to.equal(SERVER)
    });
    it("Should set RUNE", async function () {
      let tx = await BRIDGE.setRune(RUNE.address, { from: OWNER });
      expect(await BRIDGE.RUNE()).to.equal(RUNE.address)
    });

  });

  describe("User Deposit Assets", function () {

    it("Should let User Deposit RUNE", async function () {

      expect(BN2Str(await RUNE.balanceOf(USER))).to.equal(_1m);

      // await RUNE.approve(BRIDGE.address, _1m, { from: USER });
      let tx = await BRIDGE.deposit(_1m, "SWITCH:bnbaddress", { from: USER })

      expect(tx.logs[0].event).to.equal('Deposit')
      expect(BN2Str(tx.logs[0].args.value)).to.equal(_1m)
      expect(tx.logs[0].args.memo).to.equal("SWITCH:bnbaddress")

      expect(BN2Str(await RUNE.balanceOf(BRIDGE.address))).to.equal(_1m);
      expect(BN2Str(await RUNE.balanceOf(USER))).to.equal('0');
    });
  });


  describe("OWNER Transfer Assets", function () {

    it("Should transfer out RUNEs to USER2", async function () {

      expect(BN2Str(await RUNE.balanceOf(USER2))).to.equal('0');

      let tx = await BRIDGE.transferOut(USER2, '500000000000000000000000', "OUTBOUND:abcde", { from: SERVER })

      expect(tx.logs[0].event).to.equal('Outbound')
      expect(tx.logs[0].args.to).to.equal(USER2)
      expect(tx.logs[0].args.memo).to.equal("OUTBOUND:abcde")
      expect(BN2Str(tx.logs[0].args.value)).to.equal('500000000000000000000000')

      expect(BN2Str(await RUNE.balanceOf(USER2))).to.equal('500000000000000000000000');
      expect(BN2Str(await RUNE.balanceOf(BRIDGE.address))).to.equal('500000000000000000000000');
    });

  });
});
